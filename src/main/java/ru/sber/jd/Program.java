package ru.sber.jd;
import java.util.*;

public class Program{

    public static void main(String[] args) {

        Map<Integer, String> states = new HashMap<Integer, String>();
        states.put(49, "Германия");
        states.put(34, "Испания");
        states.put(33, "Франция");
        states.put(39, "Италия");

        // получим объект по ключу 49 германия
        String first = states.get(49);
        // Вывод станы с кодом 49
        System.out.println(first);
        // получим весь набор ключей
        Set<Integer> keys = states.keySet();
        // получить набор всех значений
        Collection<String> values = states.values();

        // перебор элементов
        for (Map.Entry<Integer, String> item : states.entrySet()) {

            System.out.printf("Код: %d  Страна: %s \n", item.getKey(), item.getValue());
        }

        Map<String, Person> people = new HashMap<String, Person>();
        people.put("975798456", new Person("Tom"));
        people.put("56487456", new Person("Максим"));
        people.put("23115489", new Person("Француа"));

        for (Map.Entry<String, Person> item : people.entrySet()) {

            System.out.printf("тел: %s  Имя: %s \n", item.getKey(), item.getValue().getName());
        }
    }
}
